@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
<main id="main">

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

        <div class="container-fluid page-header-outer">
            <div class="container no-padding">
                <div class="page-header col-lg-6">
                    <div class="bcircle"></div><span class="ltitle">FAQs</span>
                </div>

                <div class="page-header right-block col-lg-5 offset-1">
                    <div class="col-md-3"><div class="scircle"></div><span class="ltitle">Credits</span></div>
                    <div class="col-md-3 no-padding"><div class="scircle"></div><span class="ltitle">Purchase</span></div>
                    <div class="col-md-6 account-nav">
                        <nav class="main-nav float-right d-none d-lg-block no-padding">
                            <ul class="no-padding">
                                <li class="drop-down"><a href=""><i class="fa fa-user-circle-o" aria-hidden="true"></i>{{Auth::guard('client')->user()->user_name}}</a>
                                    <ul>
                                        <li><a href="#">Add Credits</a></li>
                                        <li><a href="#">Change Password</a></li>
                                        <li><a href="#">Feedback</a></li>
                                        <li><a href="{{url('/client/logout')}}">Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>

        <div class="container">

            <!-- <header class="section-header">
              <h3>Why choose us?</h3>
              <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p>
            </header> -->

            <div class="row padding-tb">

                <div class="col-lg-3 no-padding">
                    <div class="custom-left-block">

                        <div class="upper-left">
                            <!-- <div class="profile-pic">
                              <img src="img/dummy.jpg">
                              <div class="edit"><a href="#"><input type="file" name=""><i class="fa fa-pencil fa-lg"></i></a>
                              </div>
                            </div> -->
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: {{asset('img/dummy.jpg')}};">
                                    </div>
                                </div>
                            </div>
                            <h6>Username</h6>
                            <p>0 Credits</p>

                            <a class="update-kyc" href="#"><div class="scircle"></div><span class="ltitle">Update KYC</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="lower-left">
                            <ul id="tabs" class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a id="tab-A" href="{{url('/client/home')}}" class="nav-link"><div class="scircle"></div><label>Registration</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-B" href="{{url('/client/portfolio')}}" class="nav-link"><div class="scircle"></div><label>Portfolio</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-C" href="{{url('/client/verification')}}" class="nav-link"><div class="scircle"></div><label>Verification</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-C" href="{{url('/client/history')}}" class="nav-link active"><div class="scircle"></div><label>History</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-C" href="{{url('/client/help')}}" class="nav-link"><div class="scircle"></div><label>Help</label></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="col-lg-9 custom-right-block-outer">

                    <!-- MultiStep Form -->
                    <div class="row padding-lr-15">
                        <div class="custom-right-block">

                            <div class="row">
                                <div class="col-lg-12 display-flex">
                                    <div class="col-lg-9">
                                        <h4 class="right-title">History</h4>
                                        <h5 class="rsub-title">View all your registered documents through WritersVault.io at one place.</h5>
                                    </div>

                                    <!-- <div class="col-lg-3">
                                        <a class="add-file" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Another File</a>
                                      </div>
               -->                    </div>
                            </div>

                            <div class="divider"></div>

                            <div id="content" class="tab-content" role="tablist">
                                <div id="history" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="registration">

                                    <div id="collapse-A" class="collapse show" role="tabpanel" aria-labelledby="heading-A">
                                        <div class="card-body">
                                            <div class="container no-padding">
                                                <!-- <h2>Simple Pagination Example using Datatables Js Library</h2> -->

                                                <table class="table table-fluid borderless" id="" border="0">
                                                    <thead>
                                                    <tr class="upper-title">
                                                        <th>Filters</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th class="text-right"><a href="#">Refresh</a></th>
                                                    </tr>
                                                    <tr class="lower-title">
                                                        <th>
                                                            <div class="myselect">
                                                                <select name="slct" id="slct">
                                                                    <option selected disabled>Select Date</option>
                                                                    <option value="1">Date1</option>
                                                                    <option value="2">Date2</option>
                                                                    <option value="3">Date3</option>
                                                                </select>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div class="myselect">
                                                                <select name="slct" id="slct">
                                                                    <option selected disabled>Activity</option>
                                                                    <option value="1">Activity1</option>
                                                                    <option value="2">Activity2</option>
                                                                    <option value="3">Activity3</option>
                                                                </select>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div class="myselect">
                                                                <select name="slct" id="slct">
                                                                    <option selected disabled>Type</option>
                                                                    <option value="1">Type1</option>
                                                                    <option value="2">Type2</option>
                                                                    <option value="3">Type3</option>
                                                                </select>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div class="myselect">
                                                                <select name="slct" id="slct">
                                                                    <option selected disabled>Tokens</option>
                                                                    <option value="1">Token1</option>
                                                                    <option value="2">Token2</option>
                                                                    <option value="3">Token3</option>
                                                                </select>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                                <div class="divider1"></div>
                                                <table class="table table-fluid borderless" id="historytable" border="0">
                                                    <!-- <thead>
                                                      <tr class="upper-title">
                                                        <th>Filters</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Refresh</th>
                                                      </tr>
                                                    <tr>
                                                      <th>File Name</th>
                                                      <th><label>Date Registered</label></th>
                                                      <th><label>Certificate</label></th>
                                                      <th>Blockchain</th>
                                                    </tr>
                                                    </thead> -->
                                                    <tbody>
                                                    <tr>
                                                        <td class="td-odd">1. Draft Name 1</td>
                                                        <td class="td-even"><label>12.09.2018</label></td>
                                                        <td class="td-odd"><label>Issued</label></td>
                                                        <td class="td-even"><label>Hash number</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-odd">2. Draft Name 2</td>
                                                        <td class="td-even"><label>14.09.2018</label></td>
                                                        <td class="td-odd"><label>Issued</label></td>
                                                        <td class="td-even"><label>Hash number</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-odd">3. Draft Name 3</td>
                                                        <td class="td-even"><label>11.09.2018</label></td>
                                                        <td class="td-odd"><label>Issued</label></td>
                                                        <td class="td-even"><label>Hash number</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-odd">4. Draft Name 4</td>
                                                        <td class="td-even"><label>17.09.2018</label></td>
                                                        <td class="td-odd"><label>Issued</label></td>
                                                        <td class="td-even"><label>Hash number</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-odd">5. Draft Name 5</td>
                                                        <td class="td-even"><label>18.09.2018</label></td>
                                                        <td class="td-odd"><label>Issued</label></td>
                                                        <td class="td-even"><label>Hash number</label></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /.MultiStep Form -->

                    <!-- <div class="form-footer col-lg-6 offset-3">
                        <div class=""><a href="#">Terms of use</a></div>
                        <div class=""><a href="#">Privacy policy</a></div>
                    </div> -->

                </div>

            </div>

        </div>


        <div class="container">
            <div class="form-footer1 col-lg-3 offset-9">
                <div class=""><a href="#">Terms of use</a></div>
                <div class=""><a href="#">Privacy policy</a></div>
            </div>
        </div>


    </section>
    </main>
    @endsection

