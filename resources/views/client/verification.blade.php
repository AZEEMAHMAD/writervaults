@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
<main id="main" class="verification">

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

        <div class="container-fluid page-header-outer">
            <div class="container no-padding">
                <div class="page-header col-lg-6">
                    <div class="bcircle"></div><span class="ltitle">FAQs</span>
                </div>

                <div class="page-header right-block col-lg-5 offset-1">
                    <div class="col-md-3"><div class="scircle"></div><span class="ltitle">Credits</span></div>
                    <div class="col-md-3 no-padding"><div class="scircle"></div><span class="ltitle">Purchase</span></div>
                    <div class="col-md-6 account-nav">
                        <nav class="main-nav float-right d-none d-lg-block no-padding">
                            <ul class="no-padding">
                                <li class="drop-down"><a href=""><i class="fa fa-user-circle-o" aria-hidden="true"></i>{{Auth::guard('client')->user()->user_name}}</a>
                                    <ul>
                                        <li><a href="#">Add Credits</a></li>
                                        <li><a href="#">Change Password</a></li>
                                        <li><a href="#">Feedback</a></li>
                                        <li><a href="{{url('/client/logout')}}">Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">

            <!-- <header class="section-header">
              <h3>Why choose us?</h3>
              <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p>
            </header> -->

            <div class="row padding-tb">

                <div class="col-lg-3 no-padding">
                    <div class="custom-left-block">

                        <div class="upper-left">
                            <!-- <div class="profile-pic">
                              <img src="img/dummy.jpg">
                              <div class="edit"><a href="#"><input type="file" name=""><i class="fa fa-pencil fa-lg"></i></a>
                              </div>
                            </div> -->
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: {{asset('img/dummy.jpg')}};">
                                    </div>
                                </div>
                            </div>
                            <h6>Username</h6>
                            <p>0 Credits</p>

                            <a class="update-kyc" href="#"><div class="scircle"></div><span class="ltitle">Update KYC</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="lower-left">
                            <ul id="tabs" class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a id="tab-A" href="{{url('/client/home')}}" class="nav-link"><div class="scircle"></div><label>Registration</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-B" href="{{url('/client/portfolio')}}" class="nav-link"><div class="scircle"></div><label>Portfolio</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-C" href="{{url('/client/verification')}}" class="nav-link active"><div class="scircle"></div><label>Verification</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-C" href="{{url('/client/history')}}" class="nav-link"><div class="scircle"></div><label>History</label></a>
                                </li>
                                <li class="nav-item">
                                    <a id="tab-C" href="{{url('/client/help')}}" class="nav-link"><div class="scircle"></div><label>Help</label></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="col-lg-9 custom-right-block-outer">

                    <!-- MultiStep Form -->
                    <div class="row padding-lr-15">
                        <div class="custom-right-block">

                            <div class="row">
                                <div class="col-lg-12 display-flex">
                                    <div class="col-lg-12">
                                        <h4 class="right-title">Verification</h4>
                                        <h5 class="rsub-title">Only a file previously Verifyed on CopyrightBank can be verified. You may upload the original file or an identical copy of it.</h5>
                                    </div>


                                </div>
                            </div>

                            <div class="divider"></div>

                            <div id="content" class="tab-content" role="tablist">
                                <div id="verification" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="registration">

                                    <div id="collapse-A" class="collapse show" role="tabpanel" aria-labelledby="heading-A">
                                        <div class="card-body">
                                            <div class="container no-padding">
                                                <!-- <h2>Simple Pagination Example using Datatables Js Library</h2> -->
                                                <table class="table table-fluid borderless" id="verificationtable" border="0">
                                                    <thead>
                                                    <tr>
                                                        <th><div class="scircle"></div>&nbsp;File Name</th>
                                                        <th class="table-regtitle"><label class="">Verify All</label></th>
                                                        <th class="table-remtitle"><label>Remove All</label></th></tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1. Draft Name 1</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2. Draft Name 2</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3. Draft Name 3</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4. Draft Name 4</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5. Draft Name 5</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6. Draft Name 6</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7. Draft Name 7</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>8. Draft Name 8</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9. Draft Name 9</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9. Draft Name 9</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9. Draft Name 9</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row padding-tb-15">
                        <div class="col-lg-12 display-flex">
                            <div class="col-lg-12">
                                <!-- <a class="add-file" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Another File</a> -->
                                <div class="avatar-upload add-file-outer">
                                    <div class="avatar-edit add-file">
                                        <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload"><i class="fa fa-plus-circle" aria-hidden="true"></i>Verify Another File</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.MultiStep Form -->

                    <!-- <div class="form-footer col-lg-6 offset-3">
                        <div class=""><a href="#">Terms of use</a></div>
                        <div class=""><a href="#">Privacy policy</a></div>
                    </div> -->

                </div>

            </div>

        </div>



        <div class="container">
            <div class="form-footer1 col-lg-3 offset-9">
                <div class=""><a href="#">Terms of use</a></div>
                <div class=""><a href="#">Privacy policy</a></div>
            </div>
        </div>


    </section>
</main>
    @endsection