@extends('admin.layouts.master')
@section('content')
    <style>
        .dividerow {
            display: flex;
        }

        .dividerow > div {
            flex: 1;
            background: white;
            border: 2px solid grey;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
               <h2>Welcome to Admin Panel of <span style="color: #3c8dbc">Writers Vault</span> CMS</h2>
            </div>
        </div>
    </div>
@endsection